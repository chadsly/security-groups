resource "aws_security_group" "gitlab" {
  name        = "gitlab"
  description = "Allow public subnet to access gitlab"
  vpc_id     = var.vpcid
    ingress {
        from_port = 5432
        to_port = 5432
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    ingress {
        from_port = 6379
        to_port = 6379
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    egress {
        from_port = 0 
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
  tags = {
    Name = "gitlab"
  }
}