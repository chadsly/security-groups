resource "aws_security_group" "nessus-scanner" {
  name        = "nessus-scanner"
  description = "Allow private subnet to access nessus scanner"
  vpc_id     = var.vpcid
    ingress {
        from_port = 8834
        to_port = 8834
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    egress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    egress {
        from_port = 25
        to_port = 25
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    egress {
        from_port = 53
        to_port = 53
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
  tags = {
    Name = "nessus-scanner"
  }
}