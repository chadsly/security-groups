# Log into AWS
provider "aws" {
  #version = "~> 2.50.0"
  region = var.aws_region
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}

data "aws_subnet" "selected" {
  id = "${var.public_subnet}"
}