resource "aws_security_group" "security_center" {
  name        = "security_center"
  description = "Allow public subnet to access security center"
  vpc_id     = var.vpcid
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    ingress {
        from_port = 8834
        to_port = 8834
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    egress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    egress {
        from_port = 25
        to_port = 25
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    egress {
        from_port = 53
        to_port = 53
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
  tags = {
    Name = "security_center"
  }
}