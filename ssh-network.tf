resource "aws_security_group" "dev_ssh" {
  name        = "dev_ssh"
  description = "Allow devs to access public servers"
  vpc_id     = var.vpcid
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
  tags = {
    Name = "dev_ssh"
  }
}