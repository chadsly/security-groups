resource "aws_security_group" "consul" {
  name        = "consul"
  description = "Allow public subnet to access consul"
  vpc_id     = var.vpcid
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"] 
    }   
    ingress {
        from_port = 3000 
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"] 
    }   
    ingress {
        from_port = 443
        to_port = 443 
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
    }
    ingress {
        from_port = 8200 
        to_port = 8200
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
   }
   
    ingress {
        from_port = 8201 
        to_port = 8201
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
   }
    ingress {
        from_port = 8300 
        to_port = 8300
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
   }
    ingress {
        from_port = 8301 
        to_port = 8301
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
   }
    ingress {
        from_port = 8500 
        to_port = 8500
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
   }
    ingress {
        from_port = 8600 
        to_port = 8600
        protocol = "tcp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
   }
   ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
   }
   egress {
        from_port = 0 
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["${data.aws_subnet.selected.cidr_block}"]
   }
  tags = {
    Name = "consul"
  }
}